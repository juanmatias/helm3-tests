# Helm 3, just in time

## Why were you waiting for Helm 3?

First, because it was said that Helm 3 will solve some security issues from Helm 2. (e.g. run in a tiller-less fashion)

Second, because using Helm 2 with standard tiller, and due to a bug in *http2* library, we were having some intermittent issues. (portforward filed due to this bug, and since helm use it to connect to tiller it also fails; despite helm re tries the connection and the deployment is done, the error is sent to stderr, so the CI/CD pipeline is marked as failed)

*Yes, you can migrate to Helm 2 tiller-less, but it is a workaround instead a solution.*

## Helm 3, welcome

Let's go for a teste of it...

We will use K3d to test it.

### Create a test cluster

Create a cluster:

```
k3d create --publish 8080:80 --workers 2
export KUBECONFIG="$(k3d get-kubeconfig --name='k3s-default')"
kubectl cluster-info
```

### Get Helm 3

Download last release from `https://github.com/helm/helm/releases`.

E.g.:

```
mkdir helm3 && cd helm3
wget https://get.helm.sh/helm-v3.0.0-linux-amd64.tar.gz
wget https://get.helm.sh/helm-v3.0.0-linux-amd64.tar.gz.sha256
```

Check your file:

```
sha256sum helm-v3.0.0-linux-amd64.tar.gz | awk '{print $1}' | diff helm-v3.0.0-linux-amd64.tar.gz.sha256 - && echo OK
```

Uncompress, add new directory to PATH (locally), and get the version::

```
tar -zxvf helm-v3.0.0-linux-amd64.tar.gz
PATH=$(pwd)/linux-amd64:$PATH
helm version
```

You shoul see something like this:

```
version.BuildInfo{Version:"v3.0.0", GitCommit:"e29ce2a54e96cd02ccfce88bee4f58bb6e2a28b6", GitTreeState:"clean", GoVersion:"go1.13.4"}
```

### Install a test chart

Add repo: (by default, no repo is added to Helm 3)

```
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update
helm search repo stable
```

And install a test chart:

```
helm install myhelm3test --namespace default stable/chronograf
```

*Note that the release name is a positional parameter now.*

Once the pod is running you can check your app with a simple port-forward:

```
kubectl port-forward $(kubectl get pod | grep myhelm3test | awk '{print $1}') 8081:8888
```

And check your site on `http://localhost:8081`.

You can get some info running:

```
helm show chart stable/chronograf
```

### Can you use your Helm 2 charts?

Let's try it.

I have this *helm-fake* chart, built with Helm 2. It deploys a nginx server.

```
cd fakehelm2
helm install helm2retrocomp .
```

Seems to be working...

Now try to access the app:

```
kubectl port-forward $(kubectl get pod | grep helm2retrocomp | head -1 |  awk '{print $1}') 8081:80
```

And access your app in `http://localhost:8081/`.

So far, so good.

If you list the releases with `helm ls` you should get something like this:

```
NAME          	NAMESPACE	REVISION	UPDATED                                	STATUS  	CHART           	APP VERSION
helm2retrocomp	default  	1       	2019-11-13 16:50:03.009223672 -0300 -03	deployed	fake-0.1.0      	1.0        
myhelm3test   	default  	1       	2019-11-13 16:40:58.498566101 -0300 -03	deployed	chronograf-1.1.0	1.7.12   
```

### Deleting

For those used to use `--purge` on `helm delete`... well, you'll miss it, but is not there anymore... you just... delete... :)

### What if you already have Helm 2 with tiller?

Let's investigate it.

Delete and recreate the cluster:

```
k3d delete
k3d create --publish 8080:80 --workers 2
export KUBECONFIG="$(k3d get-kubeconfig --name='k3s-default')"
```

Open a new terminal and get Helm 2 (so you have one terminal with Helm 3 and one with Helm 2):

```
mkdir helm2 && cd helm2
wget https://get.helm.sh/helm-v2.15.0-linux-amd64.tar.gz
wget https://get.helm.sh/helm-v2.15.0-linux-amd64.tar.gz.sha256
sha256sum helm-v2.15.0-linux-amd64.tar.gz | awk '{print $1}' | diff helm-v2.15.0-linux-amd64.tar.gz.sha256 - && echo OK
tar -zxvf helm-v2.15.0-linux-amd64.tar.gz
PATH=$(pwd)/linux-amd64:$PATH
```

So, if you go to your terminal with Helm 2 and run `helm version` you should get:

```
Client: &version.Version{SemVer:"v2.15.0", GitCommit:"c2440264ca6c078a06e088a838b0476d2fc14750", GitTreeState:"clean"}
Error: could not find tiller
```

And in your Helm 3 terminal you should get:

```
version.BuildInfo{Version:"v3.0.0", GitCommit:"e29ce2a54e96cd02ccfce88bee4f58bb6e2a28b6", GitTreeState:"clean", GoVersion:"go1.13.4"}
```

Ok, we're ready to go. (before running these commands check your are running against k3d cluster... you can use `kubectl cluster-info`)

#### Helm 2

In your Helm 2 terminal run this:

```
kubectl -n kube-system create serviceaccount tiller
kubectl create clusterrolebinding tiller \
  --clusterrole=cluster-admin \
  --serviceaccount=kube-system:tiller
helm init --service-account tiller
```

After a few seconds you can run `helm version` and get:

```
Client: &version.Version{SemVer:"v2.15.0", GitCommit:"c2440264ca6c078a06e088a838b0476d2fc14750", GitTreeState:"clean"}
Server: &version.Version{SemVer:"v2.15.0", GitCommit:"c2440264ca6c078a06e088a838b0476d2fc14750", GitTreeState:"clean"}
```

Install and try our fake Helm 2 chart:

```
cd ../fakehelm2/
helm install --name helm2fromhelm2 .
kubectl port-forward $(kubectl get pod | grep helm2fromhelm2 | head -1 |  awk '{print $1}') 8081:80
```

Access `http://localhost:8081/`.

If you run `helm ls` you should get:

```
NAME          	REVISION	UPDATED                 	STATUS  	CHART     	APP VERSION	NAMESPACE
helm2fromhelm2	1       	Wed Nov 13 17:29:51 2019	DEPLOYED	fake-0.1.0	1.0        	default 
```


#### Helm 3

Install and try our fake Helm 2 chart using Helm 3:

```
cd ../fakehelm2/
helm install helm2fromhelm3 .
kubectl port-forward $(kubectl get pod | grep helm2fromhelm3 | head -1 |  awk '{print $1}') 8081:80
```

Access `http://localhost:8081/`.

If you run `helm ls` you should get:

```
NAME          	NAMESPACE	REVISION	UPDATED                                	STATUS  	CHART     	APP VERSION
helm2fromhelm3	default  	1       	2019-11-13 17:56:39.421570168 -0300 -03	deployed	fake-0.1.0	1.0      
```

## Conclusions

If you have Helm 2 installed in your cluster (tiller or tiller-less), you can start working with Helm 3 (is does not need to be "installed"). Both versions can coexist.

Once you have all your pipelines migrated to Helm 3 you can safely remove tiller from cluster.

Note you will lost your releases state. But you must evaluate whether it is afordable or not.

These can be the steps to follow to acomplish this migration:

0. Start migrating just one simple pipeline to use Helm 3. (maybe calling your binary in your agent/runner something like `helm3`)
1. If 0 has worked, go for the rest of your pipelines.
2. Once your pipelines are migrated and working you can safely delete tiller from your cluster. (and the serviceaccount you used for it)


## Notes

From `https://github.com/helm/helm/releases/tag/v3.0.0`:

```
In Helm 2, the stable chart repository was included by default. In Helm 3, no repositories are included by default. Thus, one of the first things you will need to do is add a repository. We suggest starting with https://hub.helm.sh and working from there.
```

This means that they no longer attempt to maintain an official stable charts repository. 

From the same page, they stated this:

`For the most part, though, charts that worked with Helm 2 will continue to work with Helm 3.`

Anyway, take a look to this text because you can have retro compatibility issues. (e.g. using param `-n`, previously aliasing `name`, now aliasing `namespace`)

